# Centro de Competência em Software Livre - UFPA - Website

<!-- [![pipeline status](https://gitlab.com/laai/website/badges/master/pipeline.svg)](https://gitlab.com/laai/website/commits/master) -->

Este é o repositório do site do
**Centro de Competência em Software Livre da UFPA - CCSL-UFPA**, um centro de
pesquisas da [Universidade Federal do Pará](http://ufpa.br/) vinculado ao 
[Instituto de Ciências Exatas e Naturais](http://icen.ufpa.br) e à
[Faculdade de Computação](http://computacao.ufpa.br/).

O site é baseado no [Research Group Web Site Template](https://github.com/uwsampa/research-group-web),
um tema para sites desenvolvido em [Jekyll](http://jekyllrb.com/) pelo grupo de
pesquisa [SAMPA](https://sampa.cs.washington.edu/new/index.html), da University
of Washington.

## Editando as Informações do Site

Para editar as informações do site como pessoal, notícias e projetos de
pesquisa, basta editar os arquivos texto correspondentes e em seguida recriar o
site.

### Editando Informações de Pessoal

Para editar informações de pessoal basta editar o arquivo [people.yml](_data/people.yml).

Esse arquivo lista todas as pessoas vinculadas ao CCSL-UFPA. Basta seguir o modelo
das entradas já existentes e ler as instruções sobre o significado de cada campo
no próprio arquivo.

Uma nota importante diz respeito aos nomes de orientadores (`supervisor`). O
nome deve ser **igual** ao utilizado na entrada do orientador em si nesse
arquivo. Isso é importante para gerar corretamente o link para o perfil do
orientador a partir da página do aluno orientado. A mesma regra vale para o
atributo `research`: deve ser o mesmo valor de algum atributo `name` no arquivo
dos projetos de pesquisa [research.yml](_data/research.yml).

Também é possível adicionar uma imagem para a pessoa na pasta `img`. Essa imagem
será redimensionada para 70x70px e aplicada a um filtro circular, conforme os
exemplos apresentados na página. Recomenda-se colocar uma foto **quadrada** para
evitar distorções. Atenção: a imagem não deve ter mais que 60 KB.

### Editando Informações de Projeto de Pesquisa

As informações de projetos de pesquisa estão em [research.yml](_data/research.yml).

Novamente, basta seguir o modelo presente no próprio arquivo e preencher
corretamente os campos presentes. As explicações de cada campo estão no arquivo.

Uma nota importante para esse arquivo são os nomes dos líderes de projeto
(`leader`) e os estudantes participantes do projeto (`student` - `name`). Como
no preenchimento do cadastro pessoal, os nomes do orientador do projeto e dos
estudantes devem ser **iguais** aos utilizados nos cadastros dos mesmos no
arquivo de pessoal do site. Isso permitirá que os links funcionem corretamente
entre eles após a geração do site.

### Editando Notícias

As notícias estão em [posts.yml](_data/posts.yml).

Para o site, escreva notícias curtas por volta de 2 linhas, com no máximo 3
linhas. Privilegiamos pequenos anúncios e divulgação rápida das atividades do
laboratório.

Para adicionar notícias, basta seguir o modelo indicado no arquivo. O campo
`title` é utilizado apenas na página específica de notícias e no feed RSS, não
aparecendo na página inicial do site. O campo `category` serve para colocar um
ícone na notícia. O arquivo sugere alguns mas você pode pesquisar outros ícones
no [Font Awesome](http://fontawesome.io/cheatsheet/). Quando adicionar na
notícia, apenas remova o prefixo `fa-`.

Algumas notícias de muito interesse para o site do laboratório:

* Anúncio de abertura de vagas para pesquisadores;
* Anúncio de disciplinas ofertadas pelos membros na pós ou graduação;
* Anúncio de projetos aprovados no âmbito do laboratório;
* Artigos aceitos para publicação;
* Artigos publicados;
* Chamada de trabalhos;
* Defesas dos membros do laboratório;
* Divulgação de eventos;
* Divulgação de processos seletivos;
* Entre outros.

## Como Enviar sua Colaboração

O site do CCSL-UFPA está hospedado no [Gitlab](https://gitlab.com/ccsl-ufpa/website/),
portanto é necessário antes de tudo ter instalado e saber operar o `git`.

Abaixo há algumas maneiras de como enviar uma colaboração.

### Editar via interface web do Gitlab

É possível editar diretamente os arquivos via a própria interface web do Gitlab.
Para tanto, basta ir à página que pretende editar
([people.yml](_data/people.yml), [posts.yml](_data/posts.yml) ou
[research.yml](_data/research.yml)) e clicar no botão `Edit` localizado na parte
superior à direita.

Uma tela de edição será aberta e assim é possível alterar o arquivo. Quando
tiver terminado, basta clicar no botão `Commit changes` logo abaixo do editor.
Em seguida, abra um merge request que posteriormente será revisado e inserido
no site.

### Enviar patch

Essa é a maneira mais "manual" de enviar uma colaboração, e ela não requer conta
no Gitlab.

Primeiramente, clone o repositório do website com o seguinte comando:

`$ git clone https://gitlab.com/ccsl-ufpa/website.git`

Com o repositório clonado, é possível fazer as alterações necessárias como
cadastrar/editar uma pessoa, um projeto de pesquisa ou uma notícia. Todos esses
arquivos estão na pasta `_data/`.

Após alterar, crie um patch com suas modificações. Se você tiver feito commits
das mudanças, faça um:

`$ git format-patch origin/master`

Se não tiver feito commits ainda, rode:

`$ git diff >> patch`

E envie por e-mail para [saraiva@ufpa.br](mailto:saraiva@ufpa.br) os arquivos
gerados.

<!-- Research Group Web Site Template
================================



This is a [Jekyll][]-based Web site intended for research groups. Your group should be able to get up and running with minimal fuss.

<p align="center">
<img src="screenshot.png" width="387" height="225" alt="screenshot of the template">
</p>

This project originated at the University of Washington.  You can see the machinery working live at [our site][sampa].

This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License][license].

[sampa]: http://sampa.cs.washington.edu/
[license]: https://creativecommons.org/licenses/by-nc/4.0/


Features
--------

* Thanks to [Jekyll][], content is just text files. So even faculty should be able to figure it out.
* Publications list generated from BibTeX.
* Personnel list. Organize your professors, students, staff, and alumni.
* Combined news stream and blog posts.
* Easily extensible navigation bar.
* Responsive (mobile-ready) design based on [Bootstrap][].

[Bootstrap]: http://getbootstrap.com/


Setup
-----

1. Install the dependencies. You will need [Python][], [Pybtex][] (`pip install pybtex`), and [Jekyll][] (`gem install jekyll`).
2. [Fork][] this repository on GitHub.
3. Clone the fork (along with its submodules) to your own machine: `git clone --recursive git@github.com:yourgroup/research-group-web.git`.
4. Add an "upstream" remote for the original repository so you can stay abreast of bugfixes: `git remote add upstream git://github.com/uwsampa/research-group-web.git`.
5. Customize. Start with the `_config.yml` file, where you enter the name of the site and its URL.
6. Type `make` to build the site and then run `jekyll serve -w` to view your site.
7. Keep adding content. See below for instructions for each of the various sections.
8. Periodically pull from the upstream repository: `git pull upstream master`.

[Python]: https://www.python.org/
[Fork]: https://github.com/uwsampa/research-group-web/fork


Publication List
----------------

The list of publications is in `bib/pubs.bib`. Typing `make` will generate `pubs.html`, which contains a pretty, sorted HTML-formatted list of papers. The public page, `publications.html`, also has a link to download the original BibTeX.


News Items and Blog Posts
-------------------------

For both long-form blog posts and short news updates, we use Jekyll's blogging system. To post a new item of either type, you create a file in the `_posts` directory using the naming convention `YYYY-MM-DD-title-for-url.md`. The date part of the filename always matters; the title part is currently only used for full blog posts (but is still required for news updates).

The file must begin with [YAML front matter][yfm]. For news updates, use this:

    ---
    layout: post
    shortnews: true
    ---

For full blog posts, use this format:

    ---
    layout: post
    title:  "Some Great Title Here"
    ---

And concoct a page title for your post. The body of the post goes after the `---` in either case.

[yfm]: http://jekyllrb.com/docs/frontmatter/


Personnel
---------

People are listed in a [YAML][] file in `_data/people.yml`. You can list the name, link, bio, and role of each person. Roles (e.g., "Faculty", "Staff", and "Students") are defined in `_config.yml`.

[YAML]: https://en.wikipedia.org/wiki/YAML


Building
--------

The requirements for building the site are:

* [Jekyll][]: run `gem install jekyll`
* [Pybtex][]: run `pip install pybtex`
* [bibble][]: included as a submodule. Because git is cruel, you need to use
  `git clone --recursive URL` or issue the commands `git submodule init ; git
  submodule update` to check out the dependency.
* ssh and rsync, only if you want to deploy directly.

`make` compiles the bibliography and the website content to the `_site`
directory. To preview the site, run `jekyll serve`` and head to
http://0.0.0.0:4000.


Deploying to Your Sever
-----------------------

To set up deployments, edit the Makefile and look for the lines where `HOST` and `DIR` are defined. Change these to the host where your HTML files should be copied to.

To upload a new version of the site via rsync over ssh, type `make deploy`. A web hook does this automatically when you push to GitHub. Be aware that the Makefile is configured to have rsync delete stray files from the destination directory.

[Jekyll]: http://jekyllrb.com/
[bibble]: https://github.com/sampsyo/bibble/
[pybtex]: http://pybtex.sourceforge.net -->
